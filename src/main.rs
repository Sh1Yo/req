extern crate reqwest;
use std::io;
use std::io::BufRead;

fn main() {
    for link in io::stdin().lock().lines() {
        let link = match link {
            Ok(val) => val,
            Err(_) => break,
        };

        let mut res = reqwest::Client::builder()
            .danger_accept_invalid_certs(true)
            .build()
            .unwrap()
            .get(&link)
            .send()
            .unwrap();

        let status = res.status().is_client_error();

        if !status {
            match res.text() {
                Ok(val) => println!("{}", val),
                Err(_) => continue,
            };
        }
    }
}
